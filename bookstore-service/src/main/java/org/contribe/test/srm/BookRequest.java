package org.contribe.test.srm;

import java.io.Serializable;

public class BookRequest implements Serializable {
	private static final long serialVersionUID = 5187220283668879179L;
	
	private String quantity;
	private String bookname;
	private String authorName;
	private String price;
	
	public String getQuantity() {
		return quantity;
	}
	public void setQuantity(String quantity) {
		this.quantity = quantity;
	}
	public String getBookname() {
		return bookname;
	}
	public void setBookname(String bookname) {
		this.bookname = bookname;
	}
	public String getAuthorName() {
		return authorName;
	}
	public void setAuthorName(String authorName) {
		this.authorName = authorName;
	}
	public String getPrice() {
		return price;
	}
	public void setPrice(String price) {
		this.price = price;
	}
}
