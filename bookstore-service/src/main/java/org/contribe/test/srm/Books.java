package org.contribe.test.srm;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.Date;

import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;

import org.contribe.test.srm.data.Book;
import org.contribe.test.srm.store.BookStore;

@Path("catalog")
public class Books {

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Book[] listAll() {
    	Book[] books= BookStore.getInstance().getBooks() ;
        
    	return books;
    }
    
    @GET
    @Path("{part}")
    @Produces(MediaType.APPLICATION_JSON)
    public Book[] listSearched(@PathParam("part") String part) {
    	Book[] books= BookStore.getInstance().getBooks(part) ;
        
    	return books;
    }
    
    @POST
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.TEXT_PLAIN)
	public Response addToBasket(
			@PathParam("user")String user,
			@FormParam("quantity") int quantity,
			@FormParam("bookName")String bookname,
			@FormParam("authorName")String authorName,
			@FormParam("price")String sPrice) {
    	
    	ResponseBuilder builder = Response.status(Response.Status.BAD_REQUEST);
    	builder.expires(new Date());
    	
    	DecimalFormat format = new DecimalFormat("#,##0.0#") ;
    	format.setParseBigDecimal(true);
    	BigDecimal price ;
    	try {
    		price = (BigDecimal)format.parse(sPrice) ;
    		Book book = new Book(bookname, authorName, price) ;    		
    		BookStore.getInstance().add(book, Integer.valueOf( Integer.valueOf(quantity)));
    		builder.status(Response.Status.OK).entity("Success") ;
    	} catch (Exception e) {
			price = new BigDecimal(-1) ;
			builder.status(Response.Status.BAD_REQUEST).entity(e.toString());
		}
    	return builder.build() ;
    }
}
