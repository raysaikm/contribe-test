package org.contribe.test.srm;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.contribe.test.srm.action.BookListHandler;
import org.contribe.test.srm.action.UserBasket;
import org.contribe.test.srm.data.Book;
import org.contribe.test.srm.data.BookList;

@Path("shopping-basket")
public class BookBasket {

	private static final Map<String, UserBasket> ALL_SHOPPING_BASKETS = new HashMap<>();
	
	//GET /shopping-cart: Get the shopping cart.
	@GET
    @Path("{user}")
    @Produces(MediaType.APPLICATION_JSON)
	public Collection<BookWithQuantity> getBasket(@PathParam("user")String user ) {
		UserBasket basket = ALL_SHOPPING_BASKETS.get(user) ;
		if(basket== null) {
			basket = new UserBasket() ;
			ALL_SHOPPING_BASKETS.put(user, basket);
		}
		
		List<BookWithQuantity> data = new ArrayList<>() ;
		for(Map.Entry<Book, Integer> e : basket.getBasketDetails().entrySet()) {
			data.add(new BookWithQuantity(e.getKey(),e.getValue())) ;
		}
		return data ;
	}

	public class BookWithQuantity extends Book {
		private final int quantity ;
		public BookWithQuantity(Book book, int quantity) {
			super(book.getTitle(), book.getAuthor(), book.getPrice()) ;
			this.quantity= quantity ;
		}
		
		public int getQuantity() {
			return quantity;
		}
	}
	

	//POST /shopping-cart: Add an item to the shopping cart (sending some data with the item you are adding and the amount in the request body).
    @POST
	@Path("{user}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
	public UserBasket addToBasket(@PathParam("user")String user, BookRequest req) {
		
    	System.out.println("user" + user + " quantity " + req.getQuantity() + " bookname " + req.getBookname() + " price " + req.getPrice());
    	UserBasket basket = ALL_SHOPPING_BASKETS.get(user) ;
		if(basket== null) {
			basket = new UserBasket() ;
			ALL_SHOPPING_BASKETS.put(user, basket);
		}
		Book book = new Book(req.getBookname(), req.getAuthorName(), new BigDecimal(req.getPrice())) ;
		basket.add(book, Integer.valueOf( req.getQuantity())) ; 
		return basket ;
	}

    @DELETE
	@Path("{user}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
	public UserBasket removeFromBasket(@PathParam("user")String user, BookRequest req) {
		
    	System.out.println("user" + user + " quantity " + req.getQuantity() + " bookname " + req.getBookname() + " price " + req.getPrice());
    	UserBasket basket = ALL_SHOPPING_BASKETS.get(user) ;
		if(basket== null) {
			basket = new UserBasket() ;
			ALL_SHOPPING_BASKETS.put(user, basket);
		}
		Book book = new Book(req.getBookname(), req.getAuthorName(), new BigDecimal(req.getPrice())) ;
		basket.remove(book, Integer.valueOf( req.getQuantity())) ; 
		return basket ;
	}

    
    //PUT /shopping-cart/order: Order the shopping cart content
    @PUT
	@Path("{user}")
    @Produces(MediaType.APPLICATION_JSON)
    public int[] buy( @PathParam("user")String user ) {
		UserBasket basket = ALL_SHOPPING_BASKETS.get(user) ;
		if(basket== null) {
			basket = new UserBasket() ;
			ALL_SHOPPING_BASKETS.put(user, basket);
		}
		
		BookList list = new BookListHandler() ;
		int[] data = basket.buy(list) ;
/*		boolean first = true; 
		StringBuilder s = new StringBuilder() ;
		for( int i:data ) {
			if(!first) {
				s.append(",") ;
			} else {
				first= false ;
			}
			s.append(i) ;
		}
*/		
		ALL_SHOPPING_BASKETS.remove(user) ;
		
		return data;
    }

}
