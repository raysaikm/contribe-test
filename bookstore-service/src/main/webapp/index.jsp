<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Bookstore App</title>
    <link rel="stylesheet" type="text/css" href="https://www.jeasyui.com/easyui/themes/default/easyui.css">
    <link rel="stylesheet" type="text/css" href="https://www.jeasyui.com/easyui/themes/icon.css">
    <link rel="stylesheet" type="text/css" href="../demo.css">
    <script type="text/javascript" src="https://www.jeasyui.com/easyui/jquery.min.js"></script>
    <script type="text/javascript" src="https://www.jeasyui.com/easyui/jquery.easyui.min.js"></script>
	<style>
	.container {
		width: 89%;
		background: aqua;
		margin: auto;
		padding: 10px;
	}
	.cat {
		width: 37%;
		background: aqua;
		float: left;
	}
	.basket {
		margin-left: 37%;
		background: aqua;
	}
	</style>
</head>
<body>
    <h2>Bookstore</h2>
    <p>Rudimentary UI for the contribe java test services</p>
	<p>Double-click on the catalog row to add to basket, double click on the basket row to remove</p>
    <div style="margin:20px 0;"></div>
    <section class="container">
		<div class="cat">
			<table id="catalog" title="Catalog" style="width:420px;height:300px">
				<thead>
					<tr>
						<th data-options="field:'author',width:150">Author</th>
						<th data-options="field:'title',width:150">Title</th>
						<th data-options="field:'price',width:70,align:'right'">List Price</th>
					</tr>
				</thead>
			</table>
		    <div id="toolbar1">
				<input id="searchbox"></input>
				<a id="newbookbutton" href="#">New Book(Admin only)</a>

			</div>
		</div>
		<div class="basket">
			<table id="shopping-basket" title="Basket" style="width:700px;height:300px">
				<thead>
					<tr>
						<th data-options="field:'author',width:250">Author</th>
						<th data-options="field:'title',width:250">Title</th>
						<th data-options="field:'price',width:80,align:'right'">List Price</th>
						<th data-options="field:'quantity',width:80,align:'right'">Quantity</th>
					</tr>
				</thead>
			</table>
		    <div id="toolbar2" align="center">
			    <a id="buybutton" href="#">buy</a>
			</div>
		</div>
		
		<div id="dlg" class="easyui-dialog" style="width:400px" data-options="closed:true,modal:true,border:'thin',buttons:'#dlg-buttons'">
			<!-- "bookName"			"authorName"			"price"			"quantity" -->
			<form id="fm" method="post" novalidate style="margin:0;padding:20px 50px">
				<h3>New Book Details(Admin only)</h3>
				<div style="margin-bottom:10px">
					<input name="bookName" class="easyui-textbox" required="true" label="Book Name:" style="width:100%">
				</div>
				<div style="margin-bottom:10px">
					<input name="authorName" class="easyui-textbox" required="true" label="Author :" style="width:100%">
				</div>
				<div style="margin-bottom:10px">
					<input name="price" class="easyui-textbox" required="true" label="Price" style="width:100%">
				</div>
				<div style="margin-bottom:10px">
					<input name="quantity" class="easyui-numberbox" required="true" label="Quantity:" style="width:100%">
				</div>
			</form>
		</div>
	    <div id="dlg-buttons">
			<a href="javascript:void(0)" class="easyui-linkbutton c6" iconCls="icon-ok" id='saveBook' style="width:90px">Save</a>
			<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#dlg').dialog('close')" style="width:90px">Cancel</a>
		</div>
	</section>

	
	<script type="text/javascript" src="https://www.jeasyui.com/easyui/datagrid-filter.js"></script>
    <script type="text/javascript">
		var userid = "user1" ; // hardcoded, in actual, it would be read when user logs in.
		var basketEmpty = true ;
			
		
        $(function(){
			
            var catalog = $('#catalog').datagrid({
                url: 'webapi/catalog',
				method:'get',
                pagination: true,
				toolbar:'#toolbar1',
                rownumbers: true
            });
			
			var basket = $('#shopping-basket').datagrid({
                url: 'webapi/shopping-basket/'+userid,
				method:'get',
                pagination: false,
                remoteFilter: false,
                rownumbers: true,
				showFooter: true,
				toolbar:'#toolbar2',
				onDblClickRow:function(index, row){
					sendBookToServer(row, 'DELETE');
				}
            });
			
			$('#searchbox').searchbox({
				searcher:function(value,name){
					if(value) {
						$('#catalog').datagrid('load', 'webapi/catalog/'+value  ) ;
					} else {
						$('#catalog').datagrid('load', 'webapi/catalog'  ) ;
					}
				},
				width:'55%',
				prompt:'Please Input Value to search catalog'
			});
			
			$('#buybutton').linkbutton({
				iconCls: 'icon-save'
			});
			
			$('#newbookbutton').linkbutton({
				iconCls: 'icon-add'
			});
			
			$('#catalog').datagrid({
				onDblClickRow:function(index, row){
					sendBookToServer(row, 'POST');
				}
			});
			
			function sendBookToServer(row, method) {
				$.ajax({
					data: JSON.stringify({ 
						bookname : row.title ,
						authorName : row.author ,
						price : row.price ,
						quantity : 1 
					}),
					url: 'webapi/shopping-basket/'+userid,
					contentType: "application/json",
					type:method,
					success: function(r) {
						$('#shopping-basket').datagrid('reload');
						$('#shopping-basket').datagrid('getPanel').panel('setTitle', 'Basket (' +  r.totalPrice + ')');
						basketEmpty = eval(r.empty);
						// if(basketEmpty) alert('emptied')
					}
				}) ;				
			}
			
			$('#buybutton').click( function(e) {
				if( basketEmpty ) {
					showMessage('basket is empty');
					return;
				}
				$.ajax({
					url: 'webapi/shopping-basket/'+userid,
					contentType: "application/json",
					type:'PUT',
					success: function(r) {
						$('#shopping-basket').datagrid('reload');
						$('#shopping-basket').datagrid('getPanel').panel('setTitle', 'Basket (' +  r.totalPrice + ')');
						showMessage('Buy result ' + r)
					}
				}) ;				
			})
			$('#newbookbutton').click( function(e) {
				$('#dlg').dialog('open').dialog('center').dialog('setTitle','New User');
				$('#fm').form('clear');
			})
			
			$('#saveBook').click(function(e) {
				$('#fm').form('submit',{
					url: 'webapi/catalog',
					method:'POST',
					onSubmit: function(){
						return $(this).form('validate');
					},
					success: function(result){
						$('#catalog').datagrid('load', 'webapi/catalog'  ) ;
						$('#dlg').dialog('close');        // close the dialog
						$('#dg').datagrid('reload');    // reload the user data
					},
					error: function(result){
						if (result){
							$.messager.show({
								title: 'Error',
								msg: result.errorMsg
							});
						} 
					}
				});
			})
			
			function showMessage(message) {
				$.messager.show({
					title:'Buy result',
					msg: message,
					timeout:500,
					showType:'slide',
					style:{
						right:'',
						bottom:''
					}
				});
			}
			
			$('#searchbutton').click(function(e) {
				var s = $('#searchbox')
				alert(s)
			})
			
        });
    </script>
 
</body>
</html>