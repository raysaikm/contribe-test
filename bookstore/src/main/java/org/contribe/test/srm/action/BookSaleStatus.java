package org.contribe.test.srm.action;

public enum BookSaleStatus {
	OK(0),
	NOT_IN_STOCK(1),
	DOES_NOT_EXIST(2);
	
	int code ;
	private BookSaleStatus(int code) {
		this.code = code ;
	}
	

	public int getCode() {
		return code;
	}
	
	public static BookSaleStatus findByCode(int code) {
		for( BookSaleStatus s : values() ) {
			if( s.code==code ) {
				return s ;
			}
		}
		
		return null ;
	}
}
