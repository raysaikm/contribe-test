package org.contribe.test.srm;

import java.util.Map;
import java.util.Scanner;

import org.contribe.test.srm.action.BookListHandler;
import org.contribe.test.srm.action.BookSaleStatus;
import org.contribe.test.srm.action.UserBasket;
import org.contribe.test.srm.data.Book;
import org.contribe.test.srm.data.BookList;

/**
 * Main CLI class
 * @author Saikat Roy Mahasay
 */
public class App {
	public static void main(String[] args) {
		try (Scanner in = new Scanner(System.in)) {
			String choice = null;
			BookList booklist = new BookListHandler();
			UserBasket basket = new UserBasket();
			Book[] lastSelection = null;
			while (!"q".equalsIgnoreCase(choice)) {
				System.out.println("Please put your choice"
						+ "( l to list all, s to search, a to add to bucket, r to remove, b to buy, q to quit)");
				choice = in.nextLine().toLowerCase();
				switch (choice) {
				case "l":
					lastSelection = list(booklist);
					break;
				case "a":
					add(basket, lastSelection, in);
					break;
				case "r":
					remove(basket, lastSelection, in);
					break;
				case "s":
					lastSelection = search(booklist, in);
					break;
				case "b":
					buy(booklist, basket);
					break;
				case "q":
					break;
				default:
					System.out.println("Choice '" + choice + "' not recognized");
				}
			}
		}
	}

	public static Book[] list(BookList list) {
		Book[] books = list.list("");
		int i = 0;
		for (Book b : books) {
			System.out.println((++i) + ")" + b);
		}
		return books;
	}

	public static Book[] search(BookList list, Scanner in) {
		System.out.println("Please provide the search String, terminate by newline");
		String s = in.nextLine().trim();
		Book[] books = list.list(s);
		int i = 0;
		for (Book b : books) {
			System.out.println((++i) + ")" + b);
		}
		return books;
	}

	public static void remove(UserBasket basket, Book[] books, Scanner in) {
		if( basket.isEmpty() ) {
			System.out.println("Basket is empty, remove not possible");
			return ;
		}
		int i=0;
		for(Book b: basket.getBasketDetails().keySet()) {
			System.out.println((++i) + ")" + b);
		}
		String number = "";

		while (true) {
			try {
				System.out.print("Select Book Number from last list(0 or non-numeric to exit)\nBook Number::");
				number = in.next();
				int k = Integer.parseInt(number) ;
				if( k<=0 || k>=books.length ) break ;
				System.out.print("Quantity::");
				String quantity = in.next();
				basket.remove(books[k], Integer.parseInt(quantity));
			} catch (NumberFormatException exz) {
				break;
			}
		}
	}

	public static void add(UserBasket basket, Book[] books, Scanner in) {
		if (books == null) {
			System.out.println("Please select a list by 'list' or 'search'");
			return;
		}
		int i = 0;
		for (Book b : books) {
			System.out.println((++i) + ")" + b);
		}
		String number = "";

		while (true) {
			try {
				System.out.print("Select Book Number from last list(0 or non-numeric to exit)\nBook Number::");
				number = in.next();
				int k = Integer.parseInt(number) ;
				if( k<=0 || k>=books.length ) break ;
				System.out.print("Quantity::");
				String quantity = in.next();
				basket.add(books[k], Integer.parseInt(quantity));
			} catch (NumberFormatException exz) {
				break;
			}
		}

	}

	public static void buy(BookList list, UserBasket basket) {
		int[] results = basket.buy(list);
		int i = 0 ;
		for(Map.Entry<Book, Integer> e: basket.getBasketDetails().entrySet()) {
			for(int j=0; j<e.getValue(); j++) {
				System.out.print(e.getKey());
				System.out.println( BookSaleStatus.findByCode(results[i++]) );
			}
		}

		
	}
}