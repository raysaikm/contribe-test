package org.contribe.test.srm.store;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.net.URL;
import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.contribe.test.srm.action.BookSaleStatus;
import org.contribe.test.srm.data.Book;

/**
 * The class that works as the data-storage. Programmed as lazy a singleton
 * @author Saikat Roy Mahasay
 */
public class BookStore {
	private static final String INITIAL_BOOKS_URL = "https://raw.githubusercontent.com/contribe/contribe/dev/bookstoredata/bookstoredata.txt";
	private static final String INITIAL_BOOKS_FILE = "/bookstoredata.txt";
	private Set<Book> books = new HashSet<>();
	private Map<Book, Integer> countCatalog = new HashMap<>();

	private volatile static BookStore instance;

	public static BookStore getInstance() {
		if (instance == null) {
			synchronized (BookStore.class) {
				if (instance == null) {
					instance = new BookStore();
				}
			}
		}

		return instance;
	}

	public BookStore() {
		init();
	}

	/**
	 * Reads the initial data from the URL, failing that, reads the data from the hardcoded
	 * file stored within the source
	 */
	private final void init() {
		String line;
		try {
			URL url = null ;
			try {
				url = new URL(INITIAL_BOOKS_URL);
			} catch( IOException e ) {
				url = BookStore.class.getResource(INITIAL_BOOKS_FILE) ;
				System.out.println(url);
			}
			BufferedReader reader = new BufferedReader(new InputStreamReader(url.openStream(), "utf-8"));
			// System.out.println("reader" + reader);
			while ((line = reader.readLine()) != null) {
				//System.out.println(line);
				String[] s = line.split(";");

				String title = s[0];
				String author = s[1];
				String sPrice = s[2];
				String quantity = s[3];

		    	DecimalFormat format = new DecimalFormat("#,##0.0#") ;
		    	format.setParseBigDecimal(true);

	    		BigDecimal price = (BigDecimal)format.parse(sPrice) ;
		    	
				Book book = new Book(title, author, price);
				add(book, Integer.valueOf(quantity));
			}
		} catch (Exception e) {
			// TODO: handle exception
			throw new RuntimeException(e);
		}
	}

	/**
	 * Adds one copy of a new or existing book to the store
	 * @param book the book to add
	 */
	public void add(Book book) {
		add(book, 1);
	}

	/**
	 * Adds some copies of a book to the storage
	 * @param book
	 * @param quantity
	 */
	public void add(Book book, int quantity) {
		books.add(book);
		if(countCatalog.containsKey(book)) {
			int originaQty = countCatalog.get(book) ;
			countCatalog.put(book, Integer.valueOf(quantity+originaQty));			
		} else {
			countCatalog.put(book, Integer.valueOf(quantity));
		}
	}

	/**
	 * Try to buy a single book.
	 * @param book
	 * @return proper status, as asked
	 */
	public BookSaleStatus getBook(Book book) {
		if (countCatalog.containsKey(book)) {
			int quantity = countCatalog.get(book);
			// System.out.println("###########" + book + " available quantity " + countCatalog.get(book));
			if (quantity == 0) {
				return BookSaleStatus.NOT_IN_STOCK ;
			} else {
				countCatalog.put(book, quantity - 1);
				// System.out.println("###########" + book + " available quantity " + countCatalog.get(book));
				return BookSaleStatus.OK;
			}
		} else {
			return BookSaleStatus.DOES_NOT_EXIST;
		}
	}

	/**
	 * Returns all books present in the store
	 * @return
	 */
	public Book[] getBooks() {
		return books.toArray(new Book[books.size()]);
	}

	/**
	 * Search for a book by title or author.
	 * @param string
	 * @return matching books or all, if the search string is blank
	 */
	public Book[] getBooks(String string) {
		
		if( string == null || string.trim().length() <= 0) {
			return books.toArray(new Book[books.size()]) ;
		}
		
		Set<Book> filteredBooks = books.stream()
			.filter(b -> b.getAuthor().toLowerCase().contains(string.toLowerCase()) || 
					     b.getTitle().toLowerCase().contains(string.toLowerCase()))
			.collect(Collectors.toSet()); 
	
		return filteredBooks.toArray(new Book[filteredBooks.size()]) ;
	}
	
	/**
	 * For testing only!! empty the catalog, and re-read
	 */
	public void reset() {
		books = new HashSet<>();
		countCatalog = new HashMap<>();

		this.init();
	}
}
