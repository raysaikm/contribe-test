package org.contribe.test.srm.action;

import java.util.Arrays;

import org.contribe.test.srm.data.Book;
import org.contribe.test.srm.data.BookList;
import org.contribe.test.srm.store.BookStore;

public class BookListHandler implements BookList {

	BookStore store = BookStore.getInstance();
	
	
	@Override
	public Book[] list(String searchString) {
		return store.getBooks(searchString) ;
	}

	@Override
	public boolean add(Book book, int quantity) {
		store.add(book, quantity);
		
		return false;
	}

	@Override
	public int[] buy(Book... books) {
		
		if(books == null || books.length <=0 ) return new int[0] ;
		
		return Arrays.stream(books)
			.map(store::getBook)
			.mapToInt(BookSaleStatus::getCode)
			.toArray() ;
	}
}
