package org.contribe.test.srm.action;

import java.math.BigDecimal;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.contribe.test.srm.data.Book;
import org.contribe.test.srm.data.BookList;

/**
 * Defines a shopping basket for the user.
 * @author Saikat Roy Mahasay
 *
 */
public class UserBasket {

	private Map<Book,Integer> basket = new HashMap<>() ;
	private List<Book> booksInOrder = new ArrayList<>() ;
	
	/**
	 * Add some quantities of a book to the shopping baskt
	 * @param book
	 * @param quantity
	 * @return
	 */
	public boolean add(Book book, int quantity) {

		if (basket.containsKey(book)) {
			int existingQuantity = basket.get(book);
			if (existingQuantity + quantity <= 0) {
				booksInOrder.remove(book) ;
				basket.remove(book);
			} else {
				booksInOrder.add(book) ;
				basket.put(book, existingQuantity + quantity);
			}
		} else {
			booksInOrder.add(book) ;
			basket.put(book, quantity) ;
		}
		
		return false;
	}
	
	/**
	 * Check if this basket is empty
	 * @return
	 */
	public boolean isEmpty() {
		return basket.isEmpty() ;
	}
	
	/**
	 * Remove some books from the basket 
	 * @param book
	 * @param quantity
	 * @return
	 */
	public boolean remove(Book book, int quantity) {
		return add(book, -quantity) ;
	}
	
	/**
	 * Fetch the contents of the basket
	 * @return
	 */
	public Map<Book, Integer> getBasketDetails() {
		return basket;
	}

	/**
	 * Current total price
	 * @return
	 */
	public String getTotalPrice() {
		
		return (basket == null || basket.isEmpty())?NumberFormat.getCurrencyInstance().format(0): 
			basket.entrySet().stream()
			.map(e -> e.getKey().getPrice().multiply( new BigDecimal( e.getValue() ) ) )
			.reduce( BigDecimal::add )
			.map(NumberFormat.getCurrencyInstance()::format)
			.orElse("");
		
	}
	
	/**
	 * Attempt to buy all the books in the basket
	 * @param booklist
	 * @return an int[] corressponding to each copy of a book
	 * 
	 * If the basket contains 3copies of book1 and 3 copies of book2
	 * this method will return an int[] of length 6. This is  to accomodate 
	 * for the fact that less than two copies might be available, and could 
	 * be served.
	 */
	public int[] buy(BookList booklist) {
		List<Book> books= new ArrayList<>();
		for(Book b : booksInOrder) {
			if( basket.containsKey(b) ) {
				int count = basket.get(b) ;
				for(int i=0; i<count; i++) {
					books.add(b);
				}
			}
		}

		return booklist.buy(books.toArray(new Book[books.size()]));
	}
}
