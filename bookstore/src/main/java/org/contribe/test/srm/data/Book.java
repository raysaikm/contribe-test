package org.contribe.test.srm.data;

import java.math.BigDecimal;
import java.text.NumberFormat;

public class Book {

	private final String title;
	private final String author;
	private final BigDecimal price;
	
	public Book(String title, String author, BigDecimal price) {
		super();
		this.title = title;
		this.author = author;
		this.price = price;
	}

	public String getTitle() {
		return title;
	}

	public String getAuthor() {
		return author;
	}

	public BigDecimal getPrice() {
		return price;
	}
	
	@Override
	public String toString() {
		return "Title::" + getTitle() 
				+ ", Author:" + getAuthor() + 
				", (" + NumberFormat.getCurrencyInstance().format(getPrice()) + ")";
	}
	
	@Override
	public boolean equals(Object obj) {
		return obj instanceof Book && 
				this.toString().equals(obj.toString());
	}
	
	@Override
	public int hashCode() {
		return this.toString().hashCode();
	}
}