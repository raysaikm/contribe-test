package org.contribe.test.srm;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

import java.math.BigDecimal;
import java.text.NumberFormat;

import org.contribe.test.srm.action.BookListHandler;
import org.contribe.test.srm.action.UserBasket;
import org.contribe.test.srm.data.Book;
import org.contribe.test.srm.data.BookList;
import org.contribe.test.srm.store.BookStore;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * Test various uses of the Bookstore app
 * @author Saikat Roy Mahasay
 */
public class AppTest {
	private BookStore instance ;
	private UserBasket basket = new UserBasket() ;
	private BookList booklist = new BookListHandler() ;
	
	
	private static final Book existingBook1 = new Book("How To Spend Money", "Rich Bloke", new BigDecimal(1000000.00)) ;//1
	private static final Book existingBook2 = new Book("Generic Title", "First Author", new BigDecimal(185.50));//5
	private static final Book existingBook3 = new Book("Generic Title", "Second Author", new BigDecimal(1748.00)) ;//3
	private static final Book existingBook4 = new Book("Random Sales", "Cunning Bastard", new BigDecimal(999.00)) ;//20
	

	private static final Book nonExistingBook = new Book("Non Existing Title", "Some Author", new BigDecimal(99.00)) ;//20

	
	@Before
	public void initialize() {
		basket = new UserBasket() ;
		instance = BookStore.getInstance() ;
	}
	
	@After
	public void resetStore() {
		instance.reset();
	}

	@Test
	public void bookStoreInit() {
		Book[] books = instance.getBooks() ;
		assertEquals(7, books.length);
	}
	
	@Test 
	public void search( ) {
		Book[] books = instance.getBooks("ast") ;
		assertEquals(3, books.length);
	}
	
	@Test
	public void add() {
		instance.add(new Book("Test Book1", "Test Author1", new BigDecimal(99.00)), 5);
		instance.add(new Book("Test Book2", "Test Author2", new BigDecimal(99.00)), 5);
		
		Book[] books = instance.getBooks() ;
		assertEquals(books.length, 9);
	}
	
	@Test
	public void buyExistingAllOk() {
		basket.add(existingBook1, 1) ;
		basket.add(existingBook2, 2) ;
		basket.add(existingBook3, 1) ;
		basket.add(existingBook4, 2) ;
		
		int[] expected = {0,0,0,  0,0,0} ;
		int[] actual = basket.buy(booklist) ;
		assertArrayEquals(expected, actual);
	}
	@Test
	public void buyNonExisting() {
		basket.add(existingBook1, 1) ;
		basket.add(existingBook2, 2) ;
		basket.add(nonExistingBook, 1) ;
		basket.add(existingBook4, 2) ;
		
		int[] expected = {0,0,0,  2,0,0} ;
		int[] actual = basket.buy(booklist) ;
		assertArrayEquals(expected, actual);
	}
	
	@Test
	public void buyExistingOutOfStock() {
		basket.add(existingBook1, 2) ;
		basket.add(existingBook2, 1) ;
		basket.add(existingBook3, 1) ;
		basket.add(existingBook4, 2) ;
		
		int[] expected = {0,1,0,  0,0,0} ;
		int[] actual = basket.buy(booklist) ;
		assertArrayEquals(expected, actual);
	}

	@Test
	public void priceInBasket() {
		basket.add(existingBook2, 3) ;
		String actual = basket.getTotalPrice() ;
		String expected = NumberFormat.getCurrencyInstance().format(185.50*3) ; // $556.50
		assertEquals(expected, actual);
		
		basket.add(existingBook3, 2) ;
		actual = basket.getTotalPrice() ;
		expected = NumberFormat.getCurrencyInstance().format(185.50*3 + 1748.00 * 2) ;// $4,052.50
		assertEquals(expected, actual);
		
		basket.add(existingBook1, 1) ;
		actual = basket.getTotalPrice() ;
		expected = NumberFormat.getCurrencyInstance().format(185.50*3 + 1748.00 * 2 + 1000000) ;
		assertEquals(expected, actual);
		
		
		basket.add(existingBook1, -1) ;
		actual = basket.getTotalPrice() ;		
		expected = NumberFormat.getCurrencyInstance().format(185.50*3 + 1748.00 * 2) ;// $4,052.50 again
		assertEquals(expected, actual);
		
		basket.add(existingBook3, -2) ;
		actual = basket.getTotalPrice() ;
		expected = NumberFormat.getCurrencyInstance().format(185.50*3) ; //$556.50 again
		assertEquals(expected, actual);
		
		basket.add(existingBook2, -3) ;
		actual = basket.getTotalPrice() ;
		expected = NumberFormat.getCurrencyInstance().format(0) ; // back to zero
		assertEquals(expected, actual);		

	}
	
}
