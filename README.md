#Bookstore App as asked by Contribe

###bookstore
Please try *mvn compile exec:java -Dexec.mainClass="org.contribe.test.srm.App"* to access the command line interface

###bookstore-service
The bookstore-service is a jax-rs based web service interface to the bookstore app. 

This is dependant on the bookstore app, so, in order to run this, the previous app must be *mvn install*ed. 

To run the webservices, please use *mvn compile package tomcat7:run*
The following URLs are defined - 

- http://localhost:8080/bookstore-service/webapi/catalog/ to list all bookstore.
- http://localhost:8080/bookstore-service/webapi/catalog/<<text>> to search for <<text>>.
- http://localhost:8080/bookstore-service/webapi/shopping-basket/<<user>> GET , to create a shopping cart for <<user>> or fetch the existing shopping cart, if any
- http://localhost:8080/bookstore-service/webapi/shopping-basket/<<user>> POST to add a book to the shipping cart.
- http://localhost:8080/bookstore-service/webapi/shopping-basket/<<user>> PUT to buy the books in the cart for <<user>>.


Added a very rudimentary UI. To access, please run *mvn compile package tomcat7:run* and navigate to http://localhost:8080/bookstore-service/

A jQuery based simple UI can be seen.
